# Italian translations for vdr-sudoku package.
# Copyright (C) 2007-2010 Thomas Günther <tom@toms-cafe.de>
# This file is distributed under the same license as the vdr-sudoku package.
# Diego Pierotto <vdr-italian@tiscali.it>, 2007, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: vdr-sudoku 0.3.5\n"
"Report-Msgid-Bugs-To: <tom@toms-cafe.de>\n"
"POT-Creation-Date: 2008-11-21 00:36+0100\n"
"PO-Revision-Date: 2008-11-25 20:47+0100\n"
"Last-Translator: Diego Pierotto <vdr-italian@tiscali.it>\n"
"Language-Team:  <vdr@linuxtv.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Generate a new puzzle"
msgstr "Genera nuova partita"

msgid "Load a puzzle"
msgstr "Carica partita"

msgid "Save the puzzle"
msgstr "Salva partita"

msgid "Undo last action"
msgstr "Annulla ultima azione"

msgid "Redo last action"
msgstr "Ripeti ultima azione"

msgid "Mark/unmark"
msgstr "Marca/Smarca"

msgid "Next cell"
msgstr "Cella successiva"

msgid "Next number"
msgstr "Num. successivo"

msgid "Reset the puzzle"
msgstr "Ricomincia partita"

msgid "Open setup menu"
msgstr "Apri menu opzioni"

msgid "Exit"
msgstr "Esci"

msgid "Sudoku list"
msgstr "Elenco Sudoku"

msgid "Delete the puzzle?"
msgstr "Eliminare la partita?"

msgid "Button$Load"
msgstr "Carica"

msgid "Button$Back"
msgstr "Indietro"

msgid "Edit sudoku list"
msgstr "Modifica elenco Sudoku"

msgid "Description"
msgstr "Descrizione"

msgid ""
"Congratulations!\n"
"Press OK to start a new puzzle"
msgstr ""
"Complimenti!\n"
"Premi OK per iniziare una nuova partita"

msgid "Givens count"
msgstr "Numero cifre assegnate"

msgid "Symmetric givens"
msgstr "Cifre simmetriche assegnate"

msgid "Mark errors"
msgstr "Segna errori"

msgid "Mark ambiguous numbers"
msgstr "Segna numeri ambigui"

msgid "Show possible numbers as pattern"
msgstr "Mostra num. possibili in valori"

msgid "Show possible numbers as digits"
msgstr "Mostra num. possibili in cifre"

msgid "Clear marks on reset"
msgstr "Pulisci segni al riavvio"

msgid "Key Red"
msgstr "Tasto Rosso"

msgid "Key Green"
msgstr "Tasto Verde"

msgid "Key Yellow"
msgstr "Tasto Giallo"

msgid "Large font"
msgstr "Caratteri grandi"

msgid "Large font height (pixel)"
msgstr "Altezza car. grandi (pixel)"

msgid "Large font width (pixel)"
msgstr "Larghezza car. grandi (pixel)"

msgid "Small font"
msgstr "Caratteri piccoli"

msgid "Small font height (pixel)"
msgstr "Altezza car. piccoli (pixel)"

msgid "Small font width (pixel)"
msgstr "Larghezza car. piccoli (pixel)"

msgid "Transparency (%)"
msgstr "Trasparenza (%)"

msgid "Sudoku - generate and solve Number Place puzzles"
msgstr "Sudoku - Genera e risolvi il rompicapo dei numeri"

msgid "Sudoku"
msgstr "Sudoku"
